import sys, os, requests, codecs
from lxml import html
from lxml.etree import tostring
from xml.etree import ElementTree

# Useful:
# http://docs.python-guide.org/en/latest/scenarios/scrape/
# https://www.w3schools.com/xml/xpath_intro.asp
# https://lxml.de/xpathxslt.html

##################################################################
# Initialization
#

wasabiUrl = "https://wasabi.hu/szolgaltatas/napimenu"
wasabiDivPattern = '//div[@class="large-8 columns"]'

##################################################################
# Function definitions (load* functions should return lists of strings)
#

def fetchWebpage(url):
	webpage = requests.get(url)
	webpageTree = html.fromstring(webpage.content)
	return webpage, webpageTree
# fetchWebpage

def loadWasabi():
	wasabiDay = {}
	
	wasabiPage, wasabiTree = fetchWebpage(wasabiUrl)
	wasabiDiv = wasabiTree.xpath(wasabiDivPattern)[0]
	
	# format <br> tags so they are actual newlines when output as text
	for br in wasabiDiv.xpath('*//br'):
		if br.tail:
			br.tail = '\n' + br.tail
		else:
			br.tail = '\n'
	
	wasabiBlockquotes = wasabiDiv.xpath('//blockquote')
	
	wasabiDay["monday"] = wasabiBlockquotes[0]
	wasabiDay["tuesday"] = wasabiBlockquotes[1]
	wasabiDay["wednesday"] = wasabiBlockquotes[2]
	wasabiDay["thursday"] = wasabiBlockquotes[3]
	wasabiDay["friday"] = wasabiBlockquotes[4]
	
	f = codecs.open("out.txt", "w", encoding="utf8")
	
	for key, value in wasabiDay.iteritems():
		f.write(str(key) + '\n\n')
		#f.write(tostring(value).replace(">",">\n") + '\n')
		for span in value.xpath('span'):
			f.write(span.text_content() + '\n')
		f.write('\n')
	
	f.close()
# loadWasabi

##################################################################
# Script body
#

wasabiMenu = loadWasabi()
#writeListToFile("testfile.html", wasabiMenu)